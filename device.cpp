#include "device.h"
#include <QDebug>
#include <QThread>
#include <QAbstractEventDispatcher>

device::device(QObject *parent): QObject(parent)
{

}

void device::connecttoport(){
    m_pSerialPort = new QSerialPort(this);
    m_pSerialPort -> setPortName("ttyUSB0");
    if(m_pSerialPort -> open(QIODevice::ReadWrite)){
            //Now the serial port is open try to set configuration
            if(!m_pSerialPort -> setBaudRate(QSerialPort::Baud9600))
                qDebug()<<m_pSerialPort -> errorString();

            if(!m_pSerialPort -> setDataBits(QSerialPort::Data8))
                qDebug()<<m_pSerialPort -> errorString();

            if(!m_pSerialPort -> setParity(QSerialPort::NoParity))
                qDebug()<<m_pSerialPort -> errorString();

            if(!m_pSerialPort -> setStopBits(QSerialPort::OneStop))
                qDebug()<<m_pSerialPort -> errorString();

            if(!m_pSerialPort -> setFlowControl(QSerialPort::NoFlowControl))
                qDebug()<<m_pSerialPort -> errorString();

            qDebug()<<"Serial USB0  opened!";

    }else{
        qDebug()<<"Serial USB0 not opened. Error: "<< m_pSerialPort->errorString();
    }
}

void device::closeport(){

    m_pSerialPort->close();
    qDebug()<<"Port is closed!";

}

bool device::isCancelled() {
        auto const dispatcher = QThread::currentThread()->eventDispatcher();
        if (!dispatcher) {
            qCritical() << "thread with no dispatcher";
            return false;
        }
        dispatcher->processEvents(QEventLoop::AllEvents);
        return state == IDLE;
}

void device::pause(){
    auto const dispatcher = QThread::currentThread()->eventDispatcher();
    if (!dispatcher) {
         qCritical() << "thread with no dispatcher";
         return;
    }

    if (state != RUNNING)
          return;

     state = PAUSED;
     qDebug() << "paused";
     do {
           dispatcher->processEvents(QEventLoop::WaitForMoreEvents);
     } while (state == PAUSED);
}

void device::resume(){
     if (state == PAUSED) {
          state = RUNNING;
          qDebug() << "resumed";
     }
}

void device::cancel(){
      if (state != IDLE) {
          state = IDLE;
          qDebug() << "cancelled";
      }
}
