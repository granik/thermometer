#ifndef THERMOSENSOR_H
#define THERMOSENSOR_H

#include <QObject>
#include "device.h"

class thermosensor:public device
{
    Q_OBJECT
public:
    explicit thermosensor (QObject *parent = 0)
        : device(parent)
    {

    }
public slots:
    void gettemp();
signals:
    void gettempfinished(double value);
};

#endif // THERMOSENSOR_H
