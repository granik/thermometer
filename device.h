#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>
#include <QSerialPort>
#include <QString>

class device : public QObject
{
    Q_OBJECT
public:
    device(QObject *parent = 0);
public slots:
    void connecttoport();
    void closeport();
    void pause();
    void resume();
    void cancel();

signals:
    void started();
    void finished();

protected:
    enum State { IDLE, RUNNING, PAUSED };
    State state = IDLE;
    bool isCancelled();
    QSerialPort *m_pSerialPort;
};

#endif // DEVICE_H
