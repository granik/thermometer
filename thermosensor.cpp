#include "thermosensor.h"
#include <QDebug>
#include <QThread>

void thermosensor::gettemp()
 {
     if (state == PAUSED)
         // treat as resume
         state = RUNNING;

     if (state == RUNNING)
         return;

     state = RUNNING;
     qDebug() << "started";
     emit started();


     // This loop simulates the actual work
     QString hex = "010300000001840A"; //\x01\x03\x00\x00\x00\x01\x84\n
     QByteArray cmd = QByteArray::fromHex(hex.toUtf8());

     for (auto i = 0;  state == RUNNING;  ++i) {
         QByteArray temp;
         m_pSerialPort->write(cmd);
         //qDebug() << "In the measuring cycle!";
         //the serial must remain opened
         for(int i=0;i<10;i++){
            if(m_pSerialPort->waitForReadyRead(100)){
            //Data was returned
            QByteArray data = m_pSerialPort->readAll();
            //qDebug() << "Response :"<< data;
            if (i>2 && i<5){
               temp.append(data);
            }
            }else{
            //No data
            break;
            }
          }
         //qDebug() << temp.toHex().toUInt(0,16)*0.0625;
         emit gettempfinished(temp.toHex().toUInt(0,16)*0.0625);
         QThread::msleep(100);
         if (isCancelled()) break;
          //qDebug() << i;
     }

     qDebug() << "finished";
     emit finished();
 }
