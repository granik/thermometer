#include <QCoreApplication>
#include <QDebug>
#include <QString>
#include <QtSerialPort>
#include <QSerialPortInfo>
#include <QThread>
#include "fuji.h"
#include "thermosensor.h"
#include "driver.h"
#include <QTimer>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts()){
            qDebug() << "Portname :" << info.portName();
            qDebug() << "Description  :" << info.description();
            qDebug() << "Manufacturer :"  << info.manufacturer();
    }

    Fuji *printer = new Fuji();
    QThread *thread0 = new QThread();
    thermosensor *tsensor = new thermosensor();
    driver *switcher = new driver();

    tsensor -> moveToThread(thread0);
    QObject::connect(thread0, SIGNAL(started()), tsensor, SLOT(connecttoport()));
    QObject::connect(switcher, SIGNAL(start()), tsensor, SLOT(gettemp()));
    QObject::connect(switcher, SIGNAL(pause()), tsensor, SLOT(pause()));
    QObject::connect(switcher, SIGNAL(resume()), tsensor, SLOT(resume()));
    QObject::connect(switcher, SIGNAL(cancel()), tsensor, SLOT(cancel()));
    QObject::connect(switcher, SIGNAL(closeport()), tsensor, SLOT(closeport()));
    QObject::connect(tsensor, SIGNAL(gettempfinished(double)), printer, SLOT(Print(double)));
    QObject::connect(tsensor, SIGNAL(destroyed()), thread0, SLOT(quit()));
    QObject::connect(tsensor, SIGNAL(finished()), tsensor, SLOT(deleteLater()));
    QObject::connect(tsensor, SIGNAL(finished()), tsensor, SLOT(closeport()));
    QObject::connect(thread0, SIGNAL(finished()), &a, SLOT(quit()));

    thread0 -> start();
    switcher -> start();
    QThread::sleep(1);
    switcher -> pause();
    QThread::sleep(10);
    switcher -> resume();
    //QThread::sleep(1);
    //switcher -> cancel();
    QThread::sleep(1);
    //switcher -> closeport();
    //QTimer::singleShot(5000, printer, SIGNAL(On()));

    //QTimer::singleShot(10000, printer, SIGNAL(Off()));

    //QTimer::singleShot(10000, printer, SIGNAL(On()));

    return a.exec();
}
