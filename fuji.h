#ifndef FUJI_H
#define FUJI_H

#include <QObject>

class Fuji:public QObject
{
    Q_OBJECT
public:
    Fuji();
public slots:
    void Print(double value);
    void Print(QString value);
};

#endif // FUJI_H
