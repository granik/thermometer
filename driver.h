#ifndef DRIVER_H
#define DRIVER_H

#include <QObject>

class driver:public QObject
{
    Q_OBJECT
public:
    driver();
signals:
    void connecttoport();
    void closeport();
    void start();
    void pause();
    void resume();
    void cancel();
};

#endif // DRIVER_H
